export const createTodoItem = function() {
  const btnCreateToDoitem = document.querySelector('.js-create-todo-item');
  const popupCreateToDoitem = document.querySelector('.js-popup-create-todo-item');

  const toDoForm = document.querySelector('.todo-form');
  const addInputTitle = toDoForm.querySelector('.js-add-input-title');
  const addInputDesc = toDoForm.querySelector('.js-add-input-desc');
  const addInputPriority = toDoForm.querySelector('.js-add-input-priority');
  const fields = toDoForm.querySelectorAll('.js-field');
  const btnCancelCreate = document.querySelector('.js-cancel-button');
  
  const todoList = document.querySelector('.todo-list-result');
  const priorityArray = ['low', 'normal', 'high'];
  
  btnCreateToDoitem.addEventListener('click', () => {
    popupCreateToDoitem.classList.add('-show');
  });

  class createElement {
		constructor(tag, props, ...children) {
			this.tag = tag;
			this.props = props;
			this.children = children || null;

			this.initialize();
		}

		initialize() {
			const element = document.createElement(this.tag);
			Object.keys(this.props).forEach(key => element[key] = this.props[key]);
			if (this.children.length > 0) {
        this.children.forEach(child => {
          if (typeof child === 'string') {
            child = document.createTextNode(child);
          }
          element.appendChild(child);
        });
    	}
			return element;
		}
  }
  
  function bindEvents(todoItem) {
    const doneButton = todoItem.querySelector('.js-done');
    const editButton = todoItem.querySelector('.js-edit');
    const deleteButton = todoItem.querySelector('.js-delete');

    editButton.addEventListener('click', editTodoItem);
    deleteButton.addEventListener('click', deleteTodoItem);
    doneButton.addEventListener('click', doneTodoItem);
  }

  function doneTodoItem(e) {
    const listItem = e.target.parentNode.parentNode;
    listItem.classList.add('-done');
  }

  function editTodoItem(e) {
		const listItem = e.target.parentNode.parentNode;
		const title = listItem.querySelector('.js-title');
    const desc = listItem.querySelector('.js-desc');
    const isEditing = listItem.classList.contains('-editing');
    
    const titleInput = new createElement('input', { type: 'text', className: 'js-edit-title edit-textfield textfield' }).initialize();
    title.appendChild(titleInput);
    const newEditTitle = listItem.querySelector('.js-edit-title');

    const descInput = new createElement('textarea', { className: 'js-edit-desc edit-textfield textfield' }).initialize();
    desc.appendChild(descInput);
    const newEditDesc = listItem.querySelector('.js-edit-desc');

    const changePriority = listItem.querySelector('.js-priority');
    let currentPriority;
    changePriority.addEventListener('click', () => {
      currentPriority = priorityArray.indexOf(changePriority.textContent);
      changePriority.classList.remove(`-${priorityArray[currentPriority]}`);
      currentPriority++;
      if (currentPriority >= priorityArray.length) currentPriority = 0;
      changePriority.textContent = priorityArray[currentPriority];
      changePriority.classList.add(`-${priorityArray[currentPriority]}`);
    });

    if (isEditing) {
      title.textContent = newEditTitle.value ;
      desc.textContent = newEditDesc.value;
			e.target.textContent = 'Edit';
		} else {
      titleInput.value = title.textContent;
      descInput.value = desc.textContent;
			e.target.textContent = 'Save';
    }

		listItem.classList.toggle('-editing');
	}

  function deleteTodoItem(e) {
		const listItem = e.target.parentNode.parentNode;
		todoList.removeChild(listItem);
	}

  function createTodoItem(toDoData) {
    const title = new createElement('label', { className: 'title label js-title' }, toDoData.title).initialize();
    const desc = new createElement('label', { className: 'desc label js-desc' }, toDoData.desc).initialize();
    const priority = new createElement('label', { className: `priority js-priority -${toDoData.priority}` }, toDoData.priority).initialize();
    const editButton = new createElement('button', { className: 'edit js-edit' }, 'Edit').initialize();
    const deleteButton = new createElement('button', { className: 'delete js-delete' }, 'Delete').initialize();
    const doneButton = new createElement('button', { className: 'done js-done' }, 'Done').initialize();
    const helpersElems = new createElement('div', { className: 'todo-item-helper'}, priority, editButton, deleteButton, doneButton).initialize();
    const listItem = new createElement('div', { className: 'todo-item-block', id: toDoData._id }, title, desc, helpersElems ).initialize();
    
    bindEvents(listItem);
    return listItem;
  }

  fields.forEach(inputElem => { 
    inputElem.addEventListener('focus', () => {
      inputElem.classList.remove('-error');
    });
  });

  function validForm (fields) {
    let isValid = true;
    fields.forEach(inputElem => { 
      if (inputElem.value === '') {
        inputElem.classList.add('-error');
        isValid = false;
        return
      }
    });
    return isValid
  }

  function hidePopup() {
    popupCreateToDoitem.classList.remove('-show');
    fields.forEach(inputElem => { 
      inputElem.classList.remove('-error');
      inputElem.value = '';
    });
  }

  function createNewItem (data) {
    const todoItem = createTodoItem(data);
    todoList.appendChild(todoItem);
  }

  function saveNewItem(data) {
    let savedToDoItems = {};
    let nameForNewData;

    if (localStorage.getItem('ToDoList')) {
      savedToDoItems = JSON.parse(localStorage.getItem('ToDoList'));
      nameForNewData = Object.keys(savedToDoItems).length;
    } else {
      nameForNewData = 0;
    }

    const newToDoItems = {
      [nameForNewData]: {
        data
      }
    }
    
    const newToDoItemsList = Object.assign(savedToDoItems, newToDoItems);
    localStorage.setItem('ToDoList', JSON.stringify(newToDoItemsList));
  }

  function addToDoItem (e) {
    e.preventDefault();

    if(!validForm(fields)) return

    const toDoData = {
      _id: Date.now(),
      title: addInputTitle.value,
      desc: addInputDesc.value,
      priority : addInputPriority.value
    }

    createNewItem(toDoData);
    saveNewItem(toDoData);
    
    hidePopup();
  }

  toDoForm.addEventListener('submit', addToDoItem);

  if (localStorage.getItem('ToDoList')) {
    const savedToDoItems = JSON.parse(localStorage.getItem('ToDoList'));
    Object.values(savedToDoItems).forEach(savedItem => {
      createNewItem(savedItem.data);
    });
  }

  btnCancelCreate.addEventListener('click', () => {
    hidePopup();
  });

  const filterByTextField = document.querySelector('.js-filter-by-text');
  const filterByTextBtn = document.querySelector('.js-filter-by-text-btn');
  const filterByPriorityBtn = document.querySelector('.js-filter-by-priority');
  const filterByStatusBtn = document.querySelector('.js-filter-by-status');

  filterByPriorityBtn.addEventListener('change', filterByPriority);
  filterByTextBtn.addEventListener('click', filterByText);
  filterByTextField.addEventListener('blur', filterByTextClear);

  function filterByPriority (e) {
    let targetPriority = e.target.value;
    let targetParentId = [];

    if (localStorage.getItem('ToDoList')) {
      const savedToDoItems = JSON.parse(localStorage.getItem('ToDoList'));
      Object.values(savedToDoItems).forEach(savedItem => {
        if (savedItem.data.priority === targetPriority) {
          targetParentId.push(savedItem.data._id);
        }
      });
    }

    const toDoItemBlock = document.querySelectorAll('.todo-item-block');
    toDoItemBlock.forEach(elem => {
      let _id = elem.getAttribute('id');
      if (!targetParentId.includes(+_id)) {
        elem.classList.add('-hide');
      } else {
        elem.classList.remove('-hide');
      }
    });
  }

  function filterByText () {
    let targetPriority = filterByTextField.value;
    let targetParentId = [];

    if (localStorage.getItem('ToDoList')) {
      const savedToDoItems = JSON.parse(localStorage.getItem('ToDoList'));
      Object.values(savedToDoItems).forEach(savedItem => {
        if (savedItem.data.desc === targetPriority) {
          targetParentId.push(savedItem.data._id);
        }
      });
    }

    const toDoItemBlock = document.querySelectorAll('.todo-item-block');
    toDoItemBlock.forEach(elem => {
      let _id = elem.getAttribute('id');
      if (!targetParentId.includes(+_id)) {
        elem.classList.add('-hide');
      } else {
        elem.classList.remove('-hide');
      }
    });
  }

  function filterByTextClear (e) {
    if(!e.target.value) {
      const toDoItemBlock = document.querySelectorAll('.todo-item-block');
      toDoItemBlock.forEach(elem => {
        elem.classList.remove('-hide');
      });
    }
  }
};